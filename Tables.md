# Tables

## Create a table

* The table element is used to create a table:

```html
<table>
    
</table>
```

## Table Rows

* In HTML, all the components of tables must be created

The tr element is used to create a table row:

```html
<table>
    <tr></tr>
    <tr></tr>
</table>
```

## Table Data

* Each cell element must also be defined

The table data element is used like this:

```html
<table>
    <tr><td>7</td><td>6</td></tr>
</table>
```

## Table Headings

* The th element is used to add a table heading to the table in HTML
* It must be placed within a table row

An example:

```html
<table>
  <tr>
    <th></th>
    <th scope="col">Saturday</th>
    <th scope="col">Sunday</th>
  </tr>
  <tr>
    <th scope="row">Temperature</th>
    <td>73</td>
    <td>81</td>
  </tr>
</table>
```

## Table Borders

In older versions of HTML, you could use the following code to create a border:

```html
<table border="1">
  <tr>
    <td>73</td>
    <td>81</td>
  </tr>
</table>
```

To create a border in HTML5, you can achieve the same effect in your  CSS sheet:

```css
table, td {
  border: 1px solid black;
}
```



# Tables Code Project

```html
<!DOCTYPE html>
<html>
<head>
  <title>Ship To It - Company Packing List</title>
  <link href="https://fonts.googleapis.com/css?family=Lato: 100,300,400,700|Luckiest+Guy|Oxygen:300,400" rel="stylesheet">
  <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>

  <ul class="navigation">
    <li><img src="https://s3.amazonaws.com/codecademy-content/courses/web-101/unit-9/htmlcss1-img_logo-shiptoit.png" height="20px;"></li>
    <li class="active">Action List</li>
    <li>Profiles</li>
    <li>Settings</li>
  </ul>

  <div class="search">Search the table</div>

	<table>
  	<tr>
      <th>Company Name</th>
      <td>Adam's Greenworks</td>
      <th>Number of items to Ship</th>
      <td>14</td>
      <th>Next Action</th>
      <td>Package Items</td>
    </tr>
    <tr>
    </tr>  
  </table>  

</body>
</html>
```

