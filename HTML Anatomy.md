# HTML Anatomy

* HTML is composed of elements
  * These structure the webpage, and define its content



```html
<p>
    This is a paragraph using HTML formatting
</p>
```

* In the example above, I have written a paragraph-styled sentence

  * " This is a paragraph using HTML formatting" is the CONTENT part of the code

  * The <p> & </p> are the tags (opening and closing)

  * <p>Content</p> is the element - in this case a paragraph

# The Body of HTML

