# HTML Document Standards

## Preparing for HTML

* HTML files require certain elements to set up the document properly
* You need to let the web browser know that the document is an HTML file by starting the file with a **document type declaration**

The declaration looks like this:

```html
<!DOCTYPE html>
```

* The declaration also tells the browser what version of language you are using
* The browser assumes that DOCTYPE html means HTML5, the current version of HTML
* html will always refer to the latest version of HTML (HTML6, etc)
* You need to save the file as a .html file



## The html tag

* To create HTML structure and content, opening and closing html tags must be added after declaring the doctype

For example:

```html
<!DOCTYPE html>
<html>
    <p>
        This is a paragraph
    </p>
</html>
```

## The Head

* To give the browser information about the page, the head element can be added to the document
* The head element goes above the body element
* The head element contains the *metadata* for a webpage
* Metadata is everything on the document that is not shown on the front end
* It is information about the page itself
* The opening and closing tags usually appear first after the html tag

## Page Titles

* A browser's tab displays the title specified in the title element
* Title element - part of head

Example:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>HTML Structure</title>
    </head>
    <body>
        <p>
            This is the body <br> of the document
        </p>
    </body>
</html>
```

## Linking to other webpages

* You can link to  other web pages or pages in your website using HTML5
* Also known as a *hyperlink*

Example of link code:

```html
<a href="http://link.com/">This is a link</a>
```

### Opening links in a new window

Script:

```html
<a href="https://en.wikipedia.org/wiki/Brown_bear" target="_blank">The Brown Bear</a>
```

### Relative Pages

```html
project-folder/
|—— about.html
|—— contact.html
|—— index.html
```

You can have multiple files within the website folder. You can link to these pages using this script:

```html
<a href="./contact.html">Contact</a>
```

### Linking at will

* In HTML5, a lot of elements can be used as links, not just text. An example would be an image that also has a link

Example of image link:

```html
<a href="./images.html" target="_blank"><img src="./image.png" alt="An image in the project folder"/></a>
```

### Linking to the same page

* With HTML5, we can link to different parts of the same page
* This feature uses headers
* It is useful for long pages

We need to give the target area an ID first:

```html
<p id="top">
    This is the top of the page!
</p>
```

* In this HTML code snippet, a paragraph (p) element is assigned an ID (which is also used for styling (CSS) of individual parts of the same document)
* Most elements can have an ID attached to them
* IDs should be descriptive - rather than just being called "ID1", for example, I've called the ID in the above snippet "top", which is an alright ID title
* The target link is a string containing the # character and the target element's id:

```html
<ol>
    <li><a href="#top>Top</a>"</li>
</ol>
```

## Comments

* Comments:

```html
<!-- This is a comment that the browser will not display on the front end but can be used to explain what different parts of the code mean. -->
```



# Document Standards Practice

## Code from Codecademy

```html
<!DOCTYPE html>
<html>
  
</html>  
```

## Brown bears - Codecademy

```html
<!DOCTYPE html>
<html>

<head>
  <title>Brown Bears</title>
</head>

<body>
  <a href="./index.html">Brown Bear</a>
  <a href="./aboutme.html">About Me</a>
  <h1>The Brown Bear</h1>
  <ul>
    <li><a href="#introduction">Introduction</li>
    <li><a href="#habitat">Habitat</li>
    <li><a href="#media">Media</li>
  </ul>  
  <div id="introduction">
    <h2>About Brown Bears</h2>
    <p>The brown bear (<em>Ursus arctos</em>) is native to parts of northern Eurasia and North America. Its conservation status is currently <strong>Least Concern</strong>.<br /><br /> There are many subspecies within the brown bear species, including the
      Atlas bear and the Himalayan brown bear.</p>
    <a href="https://en.wikipedia.org/wiki/Brown_bear" target="_blank">Learn More</a>
    <h3>Species</h3>
    <ul>
      <li>Arctos</li>
      <li>Collarus</li>
      <li>Horribilis</li>
      <li>Nelsoni (extinct)</li>
    </ul>
    <h3>Features</h3>
    <p>Brown bears are not always completely brown. Some can be reddish or yellowish. They have very large, curved claws and huge paws. Male brown bears are often 30% larger than female brown bears. They can range from 5 feet to 9 feet from head to toe.</p>
  </div>
  <div id="habitat">
    <h2>Habitat</h2>
    <h3>Countries with Large Brown Bear Populations</h3>
    <ol>
      <li>Russia</li>
      <li>United States</li>
      <li>Canada</li>
    </ol>
    <h3>Countries with Small Brown Bear Populations</h3>
    <p>Some countries with smaller brown bear populations include Armenia, Belarus, Bulgaria, China, Finland, France, Greece, India, Japan, Nepal, Poland, Romania, Slovenia, Turkmenistan, and Uzbekistan.</p>
  </div>
  <div id="media">
    <h2>Media</h2>
    <a href="https://en.wikipedia.org/wiki/Brown_bear" target="_blank"><img src="https://s3.amazonaws.com/codecademy-content/courses/web-101/web101-image_brownbear.jpg"/></a>
    <video src="https://s3.amazonaws.com/codecademy-content/courses/freelance-1/unit-1/lesson-2/htmlcss1-vid_brown-bear.mp4" height="240" width="320" controls>Video not supported</video>
  </div>
</body>

</html>
```

