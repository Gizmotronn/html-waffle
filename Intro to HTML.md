##### Codecademy

# HTML Anatomy

* HTML is composed of elements
  * These structure the webpage, and define its content



```html
<p>
    This is a paragraph using HTML formatting
</p>
```

* In the example above, I have written a paragraph-styled sentence

  * " This is a paragraph using HTML formatting" is the CONTENT part of the code

  * The <p> & </p> are the tags (opening and closing)

  * <p>Content</p> is the element - in this case a paragraph

# The Body of HTML

* The body is an element of HTML
* Only content INSIDE the body can be displayed to the screen
* Text, images, buttons, etc can be added to the body

Learning about the body:

```html
<h1>Liam Arbucle</h1>
<body>
  <p> "Life is very short and what we have to do must be done in the now" - Audre Lorde</p>
</body>  
```

* Elements can be nested, for example a paragraph element inside the body

# Headings

* In HTML, there are 6 types of headers - h1, h2, h3, h4, h5 and h6

# Divs/Divisions

* Divisions, or containers, divide the page into sections (menu, header, body, etc)
* They can be put in the body:

```html
<body>
    <div>
        <h1>
            Why use divs?
        </h1>
        <p>
            Great for grouping elements!
        </p>
    </div>
</body>
```

* Divs can contain any HTML element, including images

# Attributes

* Attributes are content added to the opening tag of an element

* Made up of the name and value of the attribute

* They can provide information, or change styling

* ID - a commonly used attribute

  * It can be used to specify different content (div)
  * Helpful when an element is used more than once

  Example of ID attribute:

  ```html
  <div id="menu">
          <p> This is a menu. It is wrapped inside a "div class" ID. This type of div can only be used once, as it is an ID selector. It is the same as the class selectors seen below (for example, MissingManual)</p>
      </div>
  ```

  The ID attribute is often used in conjunction with a css style sheet to create different styling for the same type of element:

  ```css
  #menu {
      /* Temporarily disabled border width border-width: 2px; */
      /* Temporarily disabled border type border-style: solid; */
      font-size: 1px;
  } /* Creates a border for the content inside the MENU ID. Also resizes it. */
  
  h1 {
      text-align: center;
      color: green;
   } /* makes all headers that are of the "h1" type green and centered */
  
  ```

  

When an attribute such as ID is added to an element (such as div), it is placed in the opening tag:

```html
<div id="intro">
    <h1>
        Introduction
    </h1>
</div>
```

# Displaying Text

* Paragraphs or spans can be used to display text
* Paragraphs contain a block of *plain text*
* Span contains short pieces of text or HTML
  * Used to separate small pieces of content that are on the same line

Span example:

```html
<div>
  <h1>Technology</h1>
</div>
<div>
  <p><span>Self-driving cars</span> are anticipated to replace up to 2 million jobs over the next two decades.</p>
</div>
```

# Styling Text

* You can also style text using HTML tags
* The em tag EMPHASISES text
* The strong tag highlights important text

* Browsers usually render em as italic, while strong is rendered as bold

Example of strong and em:

```html
<p><strong>Liam Arbuckle</strong> is the current Chairman of <em>ACORD</em></p>
```



# Line Breaks

* The spacing/indents within HTML code does not affect how the code is displayed on the front-end
* The line break (br) is used as the line break function on the front-end
* It is only composed of a starting tag, there is no ending tag

Example of line break in HTML:

```html
<p>
    This is <br> an example
</p>
```

The above code would generate a line break of the front end



# Unordered Lists

* You can display text in a list in HTML
* You can create *unordered* lists using the ul tag
* No particular order
* Bullet point
* Should not hold raw text
* Individual items must be added to the list using the li tag

Example of ul/li code:

```html
<p>
    My Games
</p>
	<ul>
    	<li>Minecraft</li>
        <li>No Man's Sky</li>
	</ul>
```

# Ordered Lists

* ol tag is used for numbered/ordered lists
* li tag is used, like in ul

Example of ol/li code:

```html
<p>
    My favourite games
</p>
<ol>
    <li>No Man's Sky</li>
    <li>Minecraft</li>
</ol>
```

# Images

* The img tag is used to add an image to a web page
* The img tag is self-closing
* The image that you are adding can be from the folder/directory that the site is stored in, or a link to an image stored online

Example of image insertion:

```html
<img src="https://s3.amazonaws.com/codecademy-content/courses/web-101/web101-image_brownbear.jpg" />
```

## Image Alts

* The alt **attribute** means alternative text
* It brings meaning to the images on the site
* Can be added to the image tag

Example:

```html
<img src="#" alt="A field of yellow sunflowers" />
```

The alt attribute serves the following purposes:

* If the image fails to load, the user can mouse over the image space on the front end to see what the image should be
* It plays a role in SEO (Search Engine Optimization)

Example of alt code: 

```html
<img src="https://s3.amazonaws.com/codecademy-content/courses/web-101/web101-image_brownbear.jpg" alt="A picture of a brown bear"/>
```

# Videos

* HTML supports displaying videos on the front end
* The video element requires an OPENING & CLOSING tag

Example of image code:

```html
<video src="myVideo.mp4" width="320" height="240" controls>Video not supported</video>
```

* The src can link to a video file hosted somewhere (for example on Youtube). In this case the link would be something like http://youtube.com/myvideo.mp4
* The video can also be stored in the website's directiry

# Example code from Codecademy

```html
<body>
  <h1>The Brown Bear</h1>
  <div id="introduction">
    <h2>About Brown Bears</h2>
    <p>The brown bear (<em>Ursus arctos</em>) is native to parts of northern Eurasia and North America. Its conservation status is currently <strong>Least Concern</strong>.<br /><br /> There are many subspecies within the brown bear species, including the Atlas bear and the Himalayan brown bear.</p>
    <h3>Species</h3>
    <ul>
      <li>Arctos</li>
      <li>Collarus</li>
      <li>Horribilis</li>
      <li>Nelsoni (extinct)</li>
    </ul>
    <h3>Features</h3>
    <p>Brown bears are not always completely brown. Some can be reddish or yellowish. They have very large, curved claws and huge paws. Male brown bears are often 30% larger than female brown bears. They can range from 5 feet to 9 feet from head to toe.</p>
  </div>
  <div id="habitat">
    <h2>Habitat</h2>
    <h3>Countries with Large Brown Bear Populations</h3>
    <ol>
      <li>Russia</li>
      <li>United States</li>
      <li>Canada</li>
    </ol>
    <h3>Countries with Small Brown Bear Populations</h3>
    <p>Some countries with smaller brown bear populations include Armenia, Belarus, Bulgaria, China, Finland, France, Greece, India, Japan, Nepal, Poland, Romania, Slovenia, Turkmenistan, and Uzbekistan.</p>
  </div>
  <div id="media">
    <h2>Media</h2>
    <img src="https://s3.amazonaws.com/codecademy-content/courses/web-101/web101-image_brownbear.jpg" alt="A Brown Bear"/>
    <video src="https://s3.amazonaws.com/codecademy-content/courses/freelance-1/unit-1/lesson-2/htmlcss1-vid_brown-bear.mp4" width="320" height="240" controls>Video not supported</video>
  </div>
</body>
```

Codecademy Account: OpusTheRobot

